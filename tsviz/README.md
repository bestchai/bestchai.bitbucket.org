TSViz is a tool for studying executions of multi-threaded systems.

* [**Try the tool!**](http://bestchai.bitbucket.io/tsviz/)
* [**Wiki**:](https://bitbucket.org/mhnnunes/tsviz/wiki/Home) explains TSViz in more detail

TSViz is a fork of [ShiViz](https://bitbucket.org/bestchai/shiviz) that integrates timestamps.