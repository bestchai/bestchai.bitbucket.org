var acceptedWebsites = ['http://jerry.nss.cs.ubc.ca/',
                        'http://kramer.nss.cs.ubc.ca:1234/',
                        'http://elaine.nss.cs.ubc.ca:1234/'];

// This function returns a random key for an array
function randomKey(arr) {
    // Math.random() returns a number between 0 and 0.99999...
    // If you multiply this value with the length of an array, you get a
    // random floating point number between 0 and that length.
    // Use Math.floor() to round it down to the next integer
    return Math.floor(Math.random() * arr.length);
}

// Select a random website from the array
var key = randomKey(acceptedWebsites);
var newLocation = acceptedWebsites[key];

// Redirect the user
window.location = newLocation;
